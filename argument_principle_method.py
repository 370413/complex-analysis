#!/usr/bin/python3

from complex_analysis import *


# Argument Principle Method
# Reference:
#   E. Anemogiannis, E. N. Glytsis, Multilayer waveguides: efficient numerical analysis of general structures, 1992

def sigma_integrand(f, P, df=None):
    try:
        return function_refs[(f, P, "sigma_integrand")]
    except KeyError:
        if df:
            r = lambda z: (z ** P) * df(z) / f(z)
        else:
            r = lambda z: (z ** P) * cauchy_derivative(f, z) / f(z)
        function_refs[(f, P, "sigma_integrand")] = r
        return r


def sigma(f, P, C, df=None):
    return TWO_PI_I_RECIP * C.integrate(sigma_integrand(f, P, df))


def no_of_roots(f, C, df=None):
    r = sigma(f, 0, C, df)
    return int(round(abs(r), 0))


def same_root_polynomial_coeffs(sigmas):
    n = len(sigmas) - 1
    coeffs = [0] * (n + 1)
    coeffs[n] = 1
    coeffs[n - 1] = -sigmas[1]
    for k in range(n - 2, 0 - 1, -1):
        tmp_sum = sum([sigmas[i] * coeffs[k + i] for i in range(1, n - k + 1)])
        coeffs[k] = (-1 / (n - k)) * tmp_sum
    return coeffs[::-1]


def complex_roots(f, C, df=None, no_of_roots_f=-1):
    if no_of_roots_f == -1:
        no_of_roots_f = no_of_roots(f, C, df)
    if no_of_roots_f == 0:
        return []
    elif no_of_roots_f > 4 or no_of_roots_f < 0:
        raise TooManyPolynomialRootsError(no_of_roots_f)

    sigmas = [no_of_roots_f]
    for i in range(1, no_of_roots_f + 1):
        sigmas.append(sigma(f, i, C, df))

    coeffs = same_root_polynomial_coeffs(sigmas)
    return list(np.roots(coeffs))


# Grid implementation

def make_grid(begin, end, real_steps=4, imag_steps=4, fun=None, max_roots=4):
    grid = []
    step = (end - begin).real / real_steps + 1j * (end - begin).imag / imag_steps
    for x in np.arange(begin.real, end.real, step.real):
        for y in np.arange(begin.imag, end.imag, step.imag):
            step_begin = x + 1j * y
            step_end = step_begin + step
            # if (step_end.real > end.real):
            #    step_end = 1j * step_end.imag + end.real
            # if (step_end.imag > end.imag):
            #    step_end = step_end.real + 1j * end.imag
            R = RectangleContour(step_begin, step_end)
            no_of_roots_f = no_of_roots(fun, R)
            if fun and no_of_roots_f > max_roots:
                subgrid = make_grid(step_begin, step_end, fun=fun, max_roots=max_roots)
                grid += subgrid
            elif not (fun and no_of_roots_f == 0):
                grid.append((R, no_of_roots_f))
    return grid


def grid_complex_roots(f, grid):
    roots = []
    for C in grid:
        roots += complex_roots(f, C[0], no_of_roots_f=C[1])
    return roots
