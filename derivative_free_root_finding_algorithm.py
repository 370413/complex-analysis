#!/usr/bin/python3

# TODO

from complex_analysis import *

def no_of_zeros(f, C):
    return TWO_PI_I_RECIP * C.image(f).integrate(lambda z : 1 / z)

def star_bilinear_form(fi, psi, f, C):
    return TWO_PI_I_RECIP * C.integrate(lambda z : fi(z) * psi(z) / f(z))
    
## Implementation of Kravanja Sakurai Barel algorithm
# Reference: P. Kravanja, T. Sakurai, M. Van Barel, 'On locating clusters of zeros of analytical functions', July 1998

def is_ok_regular_FOP(fis, r, t):
    poly_roots(next_fi)
    wat
    ...

def kravanja_sakurai_barel_algorithm(form, epsilon):
    # input: <., .>, \epsilon_{stop}
    # output: list of zeros
    N = form(1, 1)
    if !N return []
    
    mi = form(z, 1) / N
    fis = {0 : 1, 1 : lambda z : z - mi}
    r = 1
    t = 0
    while r + t < N:
        if is_ok_regular_FOP(fis, r, t): # TODO 1
            fis[r + t + 1] = regular_FOP()# TODO 2
            r = r + t + 1
            t + 0
            allsmall = True
            tau = 0
            while allsmall and r + tau < N:
                ip, maxpsum = form # TODO 3
                ip = abs(ip)
                allsmall = (ip / maxpsum) < epsilon
                tau += 1
            if allsmall:
                n = r
                return poly_roots(fis[r])
                
