#!/usr/bin/python3

# Modified version of:
# https://stackoverflow.com/a/5966088/
# Access: 2018-08-22 15:00 CEST

nodes = {
    'kronrod15': (
        [-0.991455371120813, -0.949107912342759, -0.864864423359769, -0.741531185599394, -0.586087235467691,
         -0.405845151377397, -0.207784955007898, 0.0, 0.207784955007898, 0.405845151377397, 0.586087235467691,
         0.741531185599394, 0.864864423359769, 0.949107912342759, 0.991455371120813],
        [0.022935322010529, 0.063092092629979, 0.10479001032225, 0.140653259715525, 0.169004726639267,
         0.190350578064785, 0.204432940075298, 0.209482141084728, 0.204432940075298, 0.190350578064785,
         0.169004726639267, 0.140653259715525, 0.10479001032225, 0.063092092629979, 0.022935322010529]
    ),
    'kronrod41': (
        [0.0, 0.076526521133497, 0.152605465240923, 0.227785851141645, 0.301627868114913, 0.37370608871542,
         0.443593175238725, 0.510867001950827, 0.57514044681971, 0.636053680726515, 0.693237656334751,
         0.746331906460151, 0.795041428837551, 0.839116971822219, 0.878276811252282, 0.912234428251326,
         0.940822633831755, 0.963971927277914, 0.98150787745025, 0.993128599185095, 0.998859031588278,
         -0.076526521133497, -0.152605465240923, -0.227785851141645, -0.301627868114913, -0.37370608871542,
         -0.443593175238725, -0.510867001950827, -0.57514044681971, -0.636053680726515, -0.693237656334751,
         -0.746331906460151, -0.795041428837551, -0.839116971822219, -0.878276811252282, -0.912234428251326,
         -0.940822633831755, -0.963971927277914, -0.98150787745025, -0.993128599185095, -0.998859031588278,
         0.0765265211334973, 0.227785851141645, 0.37370608871542, 0.510867001950827, 0.636053680726515,
         0.746331906460151, 0.839116971822219, 0.912234428251326, 0.963971927277914, 0.993128599185095,
         -0.076526521133497, -0.227785851141645, -0.37370608871542, -0.510867001950827, -0.636053680726515,
         -0.746331906460151, -0.839116971822219, -0.912234428251326, -0.963971927277914, -0.993128599185095],
        [0.03830035595, 0.03818893385, 0.03785224885, 0.0372914377, 0.03651534515, 0.0355272118, 0.03432433645,
         0.03291729855, 0.0313266188, 0.02955570045, 0.02759755265, 0.02547228695, 0.02321741095, 0.02083443665,
         0.0183000849, 0.0156436534, 0.0129410668, 0.01019418675, 0.00731308465, 0.00430013495, 0.00153679185,
         0.03818893385, 0.03785224885, 0.0372914377, 0.03651534515, 0.0355272118, 0.03432433645, 0.03291729855,
         0.0313266188, 0.02955570045, 0.02759755265, 0.02547228695, 0.02321741095, 0.02083443665, 0.0183000849,
         0.0156436534, 0.0129410668, 0.01019418675, 0.00731308465, 0.00430013495, 0.00153679185, 0.07637669355,
         0.07458649325, 0.07104805465, 0.0658443192, 0.059097266, 0.0509650599, 0.0416383708, 0.03133602415,
         0.0203007149, 0.00880700355, 0.07637669355, 0.07458649325, 0.07104805465, 0.0658443192, 0.059097266,
         0.0509650599, 0.0416383708, 0.03133602415, 0.0203007149, 0.00880700355]
    )
}


class Memoize(object):
    def __init__(self, func):
        self.func = func
        self.eval_points = {}

    def __call__(self, *args):
        if args not in self.eval_points:
            self.eval_points[args] = self.func(*args)
        return self.eval_points[args]


def quad_routine(func, a, b, x_list, w_list):
    """
    Does the 'heavy-lifting' of calculating the value of the straight line integral,
    by transforming the function onto [-1, 1] domain and then using Gaussian quadrature
    :param func: the integrand
    :param a: integration starting point
    :param b: integration ending point
    :param x_list: list of points on [-1, 1] where to evaluate the function values
    :param w_list: weights associated with the values of 'func' in the points in 'x_list'
    :return: integral estimate
    """
    c_1 = (b - a) / 2
    c_2 = (b + a) / 2
    func_evals = [func(c_1 * x + c_2) for x in x_list]
    return c_1 * sum([x * w for x, w in zip(func_evals, w_list)])


def quad(func, a, b, acc=1, points='kronrod41'):
    """
    Estimate the integral of 'func' on a straight line with Gaussian quadrature
    :param func: the integrand
    :param a: integration starting point
    :param b: integration ending point
    :param acc: accuracy of integration, defined as number of same-length sub-segments of
     the domain, on which to integrate separately
    :param points: name of the pre-defined list of points to use in the calculations. Must be
     a key in the 'nodes' dictionary
    :return: integral estimate
    """
    # func = Memoize(func)  # Memoize function to skip repeated function calls. TODO error estimation
    step = (b - a) / acc
    k = sum(
        [quad_routine(func, a + i * step, a + (i + 1) * step,
                      nodes[points][0], nodes[points][1])
         for i in range(acc)]
    )
    return k
