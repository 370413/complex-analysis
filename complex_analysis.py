#!/usr/bin/python3

import math
import numpy as np
import itertools
import integration as cig

## Constants

TWO_PI_I_RECIP = 1 / (2 * math.pi * 1j)

## Hash tables
# TODO rewrite as classes (OOP)

# stores precalculated values of integrals over straight line segments
# form: { function: { segment: value, ...} }
segments_integrals_hashtbl = {}

# stores function references for use as hash keys in 
# 'segments_integrals_hashtbl'
function_refs = {}


## Classes

class TooManyPolynomialRootsError(Exception):
    def __init__(self, n):
        """
        Raised when the number of roots of polynomial is too high
        :param n: number of polynomial roots
        """
        self.n = n


class Segment:
    def __init__(self, begin, end):
        """
        Straight segment in complex plane
        :param begin: starting point of the segment
        :param end: ending point of the segment
        """
        self.begin = begin
        self.end = end

    def __repr__(self):
        return str(self.begin) + " -- " + str(self.end)

    def integrate(self, f):
        """
        Integrates function on the segment
        :param f: the integrand
        :return: definite integral on the segment
        """
        return cig.quad(f, self.begin, self.end)

    def image(self, f, N = 20):
        """
        Creates a polygon contour that approximates the image of the segment
        under transformation 'f'
        :param f: transformation
        :param N: accuracy (number of segments in resulting polygon contour)
        :return: approximation of f(self) (PolygonContour)
        """
        points = [
            f((1 - x) * self.begin + x * self.end)
            for x in np.linspace(0, 1, num=N)
            # TODO spacing should change dynamically to correspond to the weak
            # derivative of the function along the original segment
            ]
        r = []
        for i in range(N - 1):
            r.append(Segment(points[i], points[i + 1]))
        return PolygonContour(r)


class CircleContour:
    def __init__(self, centre, radius):
        """
        A circle in complex plane
        :param centre: centre
        :param radius: radius
        """
        self.centre = centre
        self.radius = radius

    def integrate(self, f):
        """
        Integrates funtion on the circle
        :param f: the integrand
        :return: definite integral
        """
        def integrand(t):
            z = self.centre + self.radius * math.e ** (1j * t)
            return f(z) * 1j * self.radius * math.e ** (1j * t)
        return cig.quad(integrand, 0, 2 * math.pi)


class PolygonContour:
    def __init__(self, list_of_segments):
        """
        Polygon (for now just a set of segments) in complex plane
        :param list_of_segments: segments constituting the polygon
        """
        self.list_of_segments = list_of_segments

    def __repr__(self):
        return str(self.list_of_segments)

    def image(self, f, N = 20):
        """
        Creates a polygon contour that approximates the original polygon under
        transformation 'f'
        :param f: transformation
        :param N: accuracy (number of segments polygon contour to which any
        segment is transformed)
        :return: curve f(self) approximated with straight line segments
        """
        new_list = [segment.image(f) for segment in self.list_of_segments]
        return PolygonContour(list(itertools.chain.from_iterable(new_list)))

    def integrate(self, f):
        """
        Integrate function on the contour
        :param f: the integrand
        :return: definite integral of 'f' on the contour
        """
        result = 0j

        try:
            hashtbl = segments_integrals_hashtbl[f]
        except KeyError:
            segments_integrals_hashtbl[f] = {}
            hashtbl = segments_integrals_hashtbl[f]
        for segment in self.list_of_segments:
            try:
                result += hashtbl[segment]
            except KeyError:
                r = segment.integrate(f)
                hashtbl[segment] = r
                result += r
        return result


class RectangleContour(PolygonContour):
    def __init__(self, ld, ru):
        """
        A rectangle with sides parallel to the axes in complex plane
        :param ld: left down corner
        :param ru: right upper corner
        """
        lu = ld.real + 1j * ru.imag
        rd = 1j * ld.imag + ru.real
        self.list_of_segments = [
            Segment(lu, ld),
            Segment(ld, rd),
            Segment(rd, ru),
            Segment(ru, lu)
        ]


## Functions
# General complex analysis

def cauchy_derivative(f, point, R = 1e-2):
    """
    Calculates a value of a function derivative in a point using
    Cauchy's integral formula
    Reference:
        Weisstein, Eric W. "Cauchy Integral Formula." From MathWorld--A Wolfram Web Resource. http://mathworld.wolfram.com/CauchyIntegralFormula.html
    :param f: the function
    :param point: point in which to calculate the derivative
    :param R: radius to use for the contour around the 'point'.
    A circle centered on point and with radius R cannot contain poles.
    :return: f'(point)
    """

    #def integrand(t):
    #    z = point + R * math.e ** (1j * t)
    #    return f(z) * 1j * R * math.e ** (1j * t) / ((z - point) ** 2)

    #return TWO_PI_I_RECIP * cig.quad(integrand, 0, 2 * math.pi)

    C = CircleContour(point, R)
    return TWO_PI_I_RECIP * C.integrate(lambda z : f(z) / ((z - point) ** 2))
